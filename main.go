package main

import (
	"fmt"
)

func Solution(A []int) int {
	if len(A)%2 == 0 {
		panic("Не может в массиве с четной длинной\n быть элемент без пары")
	}
	var res int
	for i := 0; i < len(A); i++ {
		j := i
		for ; j > 0 && A[i] < A[j-1]; j-- {
		}
		for ; i > j; i-- {
			A[i], A[i-1] = A[i-1], A[i]
		}
	}
	fmt.Println(A)
	len := len(A)

	for k := len / 2; k != 0; k-- {
		if A[k-1] < A[k] && A[k] < A[k+1] {
			res = A[k]
		}
	}
	if res == 0 {
		for i := len / 2; i != len; i++ {
			if A[i-1] < A[i] && A[i] < A[i+1] {
				res = A[i]
			}
		}
	}
	return res
}

func main() {
	A := []int{10, 9, 6, 9, 6, 10, 7, 1, 9, 11, 9, 1, 6, 5, 5, 1, 1, 6, 11}

	res := Solution(A)

	fmt.Println(res)
}
